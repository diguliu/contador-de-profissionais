require 'csv'

files = Dir.glob('planilhas/*.csv')
processados = 0
falhas = 0

files.each do |file|
  lista = {}
  basename = File.basename(file)
  next unless Dir.glob("resultados/#{basename}").empty?
  print "+ #{basename}: "
  begin
    CSV.foreach(file) do |row|
      cbo = row[4]
      sus = row[5]
      next if  cbo == '' || cbo == nil || cbo == 'CBO'
      cbo.gsub!("\n", ' ')
      if lista[cbo].nil?
        lista[cbo] = {:quantidade => 0}
      end
      lista[cbo][:quantidade] += 1

      if sus.nil?
        sus_valor = 'n'
      else
        sus_valor = sus.downcase[0].to_sym
      end

      if lista[cbo][sus_valor].nil?
        lista[cbo][sus_valor] = 0
      end
      lista[cbo][sus_valor] += 1
    end

    lista = lista.select {|key, value| !key.split(' - ')[1].nil?}
    lista = lista.sort_by {|key, value| key.match('-') ? key.to_s.split(' - ')[1] : key.to_s.split(' – ')[1]}
    total = 0

    CSV.open("resultados/#{basename}", "wb") do |csv|
      csv << ['CBO', 'Quantidade', 'SUS Sim', 'SUS Nao']
      lista.each do |cbo, hash|
        total += hash[:quantidade]
        csv << [cbo, hash[:quantidade], hash[:s] || 0, hash[:n] || 0]
      end

      csv << ['', '', '', '']
      csv << ['TOTAL', total]
    end
    puts "OK"
    processados += 1
  rescue Exception => exception
    erros = File.open("erros/#{basename.split('.')[0]}.txt", 'w+')
    erros.write(exception.to_s)
    erros.write("\n\n")
    erros.write(exception.backtrace.join("\n"))
    erros.close
    puts "deu ruim!"
    falhas += 1
  end
end

if processados == 0 && falhas == 0
  puts "[info] Nenhum novo arquivo para ser processado!"
elsif processados - falhas > 0
  puts "[info] #{processados - falhas} novo(s) arquivo(s) processado(s)!"
end

if falhas > 0
  puts "[erro] Ocorreu #{falhas} falha(s)! Confira os logs de erros armazenados na pasta 'erros' para mais detalhes."
end

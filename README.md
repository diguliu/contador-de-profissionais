# O que é esse script?

Esse script foi feito para contar o número de profissionais de cada CBO em um
arquivo CSV convertido pelo site pdftables.com a partir de um PDF gerado no
CNES. Qualquer outro CSV que seja gerado de outra forma não tem nenhuma
garantia de funcionar, a não ser que a estrutura do CSV seja idêntica à gerada
por esse processo. Mesmo arquivos CSV gerados a partir desse processo podem
apresentar falhas, apesar de que boa parte dos problemas já são tratados pelo
script.

Após a execução, o script gera um novo CSV contendo todos os CBO's encontrados
ordenados alfabeticamente, com o número de profissionais de cada um, bem como o
número de profissionais que atendem e não atendem o SUS. Além disso, o CSV
também contém no final o número total de profisisonais, valor este que serve de
referência para comparar com o PDF original e confirmar que o resultado foi
gerado corretamente.

# Dependências

Para executar esse script é recomendável que você use o ruby versão 2.3.3. O
script deve funcionar em qualquer versão maior ou igual a essa, porém ele não
foi testado em outras versões.

# Como usar o script?

Para usar o script, basta colocar os arquivos CSV gerados dentro de uma pasta
de nome `planilhas` no mesmo nível do script e rodar o seguinte comando:

````
ruby contador-de-profissionais.rb
```

Após executar esse comando, o script irá executar a contagem de cada arquivo e
gerar um novo arquivo com o mesmo nome em uma pasta de nome `resultados` com os
resultados da contagem.

Caso um arquivo CSV dentro da pasta `planilhas` já tem tenha seu resultado
calculado dentro da pasta `resultados`, ele não será contabilizado novamente.
Se você deseja recalcular algum arquivo, remova o resultado relativo a ele da
pasta de resultados.

Durante a execução, o script imprime na tela os arquivos que estão sendo
convertidos, sinalizando com um `Ok` caso a contabilização aconteça sem nenhum
problema ou `deu ruim!` caso algum problema aconteça. No caso de problema, o
script simplesmente ignora o arquivo problemático registrando na pasta `erros`
o erro ocorrido e continua a execução dos demais arquivos.

Juntamente com o script estão disponibilizados algumas planilhas e alguns
resultados já contabilizados pelo script que podem servir de referẽncia para
uso do script.

# Alertas

Esse script foi escrito de forma completamente casual e não profissional. O
autor dele não garante, nem se responsabiliza por nenhum mal funcionamento que
ele possa apresentar, tanto do ponto de vista de veracidade dos dados quanto de
danos ao seu sistema de arquivos ou sistema operacional. Ao usar esse script,
você está assumindo total responsabilidade!

# Autor

Rodrigo Souto

# Licença

Esse script está licenciado sob a licença AGPLv3. Uma cópia da licença está
disponibilizada juntamente com ele com o nome `LICENSE.md`.

Use software livre!  ;)
